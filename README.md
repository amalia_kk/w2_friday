# Ha proxy instructions

### Suggested sequence of steps

- Start the 3 machines
- Tag them 
- Use previous scripts to make 2 nginx machines
    - If need be, manually nano index.html file and write Server 1 on one machine and Server 2 on other, whatever you wanna write so you know which is which 
  
Focus on HA proxy

- First read
- Try it out so you know how to do it manually
- Automate



## Okay this is what I did

- Went to https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Instances:v=3 (if you do this the long way, make sure you're logged into Amalia)
- Click 'launch instances'
- Select the machine you want to set up 
- Keep clicking next until you get to tags: Key = Name and Value = Whateveryouwannacallit
- Next, select existing security group, if you're at home select Wizard 1, if at Anna's do 2/3
- Keep pressing next then when it asks you about the key, click the dropdown box to ensure you select the correct key
- Then when your machine/s all set up, click on it and find the Public IPv4 address and use this to log into ubuntu (or whichever one you're using) using that IP address
- Next install nginx on server 1 and 2 (you've got the steps in a bash script in week2/Tue/Ubuntu)
- Then install on the loadbalancer server haproxy using this site: https://www.haproxy.com/blog/how-to-install-haproxy-on-ubuntu/
- Follow all of the steps then go to this website: https://upcloud.com/community/tutorials/haproxy-load-balancer-ubuntu/ , click <u>Load Balancing on Layer 4</u> and copy and paste the first line of code 
- Then paste at the bottom of the text that's already there:
  
  ```bash
  frontend http_front
   bind *:80
   stats uri /haproxy?stats
   default_backend http_back
   backend http_back
   balance roundrobin
   server <server1 name> <private IP 1>:80 check
   server <server2 name> <private IP 2>:80 check
   ```
   
    But replace <server1 name> and <server2 name> with your sever names (in this case server1 and server2, remove the pointy brackets) and paste in the private IP addresses of each server in the space next to your server names
- Then paste ```sudo systemctl restart haproxy``` then ```sudo systemctl status haproxy``` to check that it shows that it was recently active
- Maybe now when you enter the ip for the loadbalancer and refresh a couple of times it'll switch betw. server1 and 2
  
  


